/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
module tagtool is aliced;
private:

import core.exception : ExitException;

import iv.taglib;
import iv.writer;


string utf2koi (const(char)[] s) {
  import std.encoding : EncodingScheme, INVALID_SEQUENCE;
  import iv.encoding;
  auto efrom = EncodingScheme.create("utf-8");
  auto eto = EncodingScheme.create("koi8-u");
  ubyte[] res;
  ubyte[8] buf;
  const(ubyte)[] ub = cast(const(ubyte)[])s;
  while (ub.length > 0) {
    dchar dc = efrom.safeDecode(ub);
    if (dc == INVALID_SEQUENCE) dc = '?';
    eto.encode(dc, buf);
    res ~= buf[0];
  }
  return cast(string)res;
}


string koi2utf (const(char)[] s) {
  import std.encoding : EncodingScheme, INVALID_SEQUENCE;
  import iv.encoding;
  auto efrom = EncodingScheme.create("koi8-u");
  auto eto = EncodingScheme.create("utf-8");
  ubyte[] res;
  ubyte[8] buf;
  const(ubyte)[] ub = cast(const(ubyte)[])s;
  while (ub.length > 0) {
    dchar dc = efrom.safeDecode(ub);
    if (dc == INVALID_SEQUENCE) dc = '?';
    auto eclen = eto.encode(dc, buf);
    res ~= buf[0..eclen];
  }
  return cast(string)res;
}


void showTags (string fname) {
  auto tf = TagFile(fname);
  if (tf.artist.length) writeln("ARTIST=", utf2koi(tf.artist));
  if (tf.album.length) writeln("ALBUM=", utf2koi(tf.album));
  if (tf.title.length) writeln("TITLE=", utf2koi(tf.title));
  if (tf.genre.length) writeln("GENRE=", utf2koi(tf.genre));
  if (tf.year) writeln("YEAR=", tf.year);
  if (tf.track) writeln("TRACKNUMBER=", tf.track);
  if (tf.comment.length) writeln("COMMENT=", utf2koi(tf.comment));
}


void main (string[] args) {
  bool optAdd = false;
  bool optTags = false;
  string fileName;

  void usage (string opt=null) {
    write("
usage: tagtool [--add] filename [tagfile]
       tagtool [--add] [--tags] filename [name=value]
short options:
  -a  --add
  -t  --tags
");
    throw new ExitException();
  }

  static uint s2i (string s) {
    import std.ascii;
    if (s.length == 0) return 0;
    uint res = 0;
    while (s.length) {
      if (!isDigit(s[0])) return 0;
      res = res*10+s[0]-'0';
      s = s[1..$];
    }
    return res;
  }

  try {
    import std.getopt;
    getopt(args, std.getopt.config.caseSensitive, std.getopt.config.bundling, std.getopt.config.stopOnFirstNonOption,
      "add|a", &optAdd,
      "tags|t", &optTags,
      "help|h", &usage
    );
  } catch (Exception e) {
    errwriteln("FATAL: ", e.msg);
    throw new ExitException();
  }
  if (args.length < 2) usage();
  fileName = args[1];
  args = args[2..$];

  try {
    if (args.length == 0) {
      showTags(fileName);
    } else {
      string artist, album, title, genre, comment;
      uint year, track;
      bool artistChanged, albumChanged, titleChanged, genreChanged, commentChanged, yearChanged, trackChanged;
      if (optAdd) {
        // load original tags
        auto tf = TagFile(fileName);
        if (tf.artist.length) artist = tf.artist;
        if (tf.album.length) album = tf.album;
        if (tf.title.length) title = tf.title;
        if (tf.genre.length) genre = tf.genre;
        if (tf.comment.length) comment = tf.comment;
        if (tf.year) year = tf.year;
        if (tf.track) track = tf.track;
        debug {
          writeln("optadd!");
          writeln(" ARTIST=", utf2koi(artist));
          writeln(" ALBUM=", utf2koi(album));
          writeln(" TITLE=", utf2koi(title));
          writeln(" GENRE=", utf2koi(genre));
          writeln(" COMMENT=", utf2koi(comment));
          writeln(" YEAR=", year);
          writeln(" TRACKNUMBER=", track);
        }
      }

      void processLn (string v) {
        import std.string : indexOf, strip, toUpper;
        v = koi2utf(v.idup);
        v = v.strip;
        if (v.length == 0 || v[0] == ';' || v[0] == '#') return;
        auto ep = v.indexOf('=');
        if (ep < 1) return;
        string name = v[0..ep].toUpper;
        v = v[ep+1..$].strip;
        debug writeln("<", name, ">=<", v, ">");
        uint tmp;
        switch (name) {
          case "ARTIST": artistChanged = (artist != v); artist = v; break;
          case "ALBUM": albumChanged = (album != v); album = v; break;
          case "TITLE": titleChanged = (title != v); title = v; break;
          case "GENRE": genreChanged = (genre != v); genre = v; break;
          case "COMMENT": commentChanged = (comment != v); comment = v; break;
          case "YEAR": case "DATE": tmp = s2i(v); yearChanged = (year != tmp); year = tmp; break;
          case "TRACK": case "TRACKNUMBER": tmp = s2i(v); trackChanged = (track != tmp); track = tmp; break;
          default:
        }
      }

      if (optTags) {
        foreach (tag; args) processLn(tag);
      } else if (args[0] != "-") {
        foreach (fname; args) {
          import std.stdio : File;
          foreach (auto ln; File(fname, "r").byLine) processLn(cast(string)ln);
        }
      } else {
        import std.stdio : stdin;
        foreach (auto ln; stdin.byLine) processLn(cast(string)ln);
      }
      debug {
        writeln("final:");
        writeln(" ARTIST=", utf2koi(artist));
        writeln(" ALBUM=", utf2koi(album));
        writeln(" TITLE=", utf2koi(title));
        writeln(" GENRE=", utf2koi(genre));
        writeln(" COMMENT=", utf2koi(comment));
        writeln(" YEAR=", year);
        writeln(" TRACKNUMBER=", track);
      }
      // write it
      if (artistChanged || albumChanged || titleChanged || genreChanged || commentChanged || yearChanged || trackChanged) {
        auto tf = TagFile(fileName);
        tf.artist = artist;
        tf.album = album;
        tf.title = title;
        tf.genre = genre;
        tf.comment = comment;
        tf.year = year;
        tf.track = track;
        tf.save();
      }
    }
  } catch(Exception e) {
    debug writeln("EXPECTION: ", e.msg);
  }
}
